import requests 
from bs4 import BeautifulSoup
import csv

#Request web content
result = requests.get('https://www.theguardian.com/news/datablog/2012/feb/03/malaria-deaths-mortality')

#Save content in variable
src = result.content

#soup activate
soup = BeautifulSoup(src,'lxml')

#Look for table and save into csv
table = soup.find('table')
with open ('malariadeath.csv', 'w', newline='') as f:
    writer = csv.writer(f)
    writer.writerow(('Country','Malaria Death 1980(per 1000 population)','Malaria Death 1990(per 1000 population)','Malaria Death 2000(per 1000 population)','Malaria Death 2010(per 1000 population)'))
    for tr in table('tr')[2:]:
        row = [t.get_text(strip=True) for t in tr(['td','th'])]
        cell1 = row[0]
        cell2 = row[1].replace('<','')
        cell3 = row[2].replace('<','')
        cell4 = row[3].replace('<','').replace('.','')
        cell5 = row[4].replace('<','').replace('.','')
        row = [cell1,cell2,cell3,cell4,cell5]
        writer.writerow(row)