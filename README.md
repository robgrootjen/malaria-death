The datapackage shows how many people die in 1980, 1990, 2000 and 2010 from Malaria in each country.

## Data
The data is sourced from: 
* Historical data table per country - https://www.theguardian.com/news/datablog/2012/feb/03/malaria-deaths-mortality

The repo includes:
* datapackage.json
* malariadeath.csv
* 1 scrapers.
    * malariascraper.py
--------------------------------------------------------------------------------------------------------------------------------

## Preparation
Requires:
1. Python
2. Visualstudio / Jupyter notebook / or any platform that works with python.

***malariadeath.csv*** (Shows how many people die from Malaria in 1980, 1990, 2000, 2010 in each country.)

`IMPORTANT` Value '0.1' has '<='. '<=0.1'. Removed for coding purposes
* The CSV Data has 5 colummns. 
    * Country
    * Malaria Death 1980(per 1000 population)
    * Malaria Death 1990(per 1000 population)
    * Malaria Death 2000(per 1000 population)
    * Malaria Death 2010(per 1000 population)
    

***datapackage.json***
* The json file has all the information from the two csv files, licence, author and 2 line graphs for every table.

***malariascraper.py***
* This script will scrape the current table from 1980, 1990, 2000, 2010 how many die in each country from  - 
https://www.theguardian.com/news/datablog/2012/feb/03/malaria-deaths-mortality
    
***Instructions:***
* Copy script in the "Process Scripts" folder
* Open in jupyter notebook, python shell, VS code, or any preferred platform.
* Run the code
* Csv file will be saved in document where your terminal is at the moment.
----------------------------------------------------------------------------------------------------------------------------------------

## Licence
This Data Package is made available under the Public Domain Dedication and License v1.0 
